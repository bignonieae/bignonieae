library(googledrive)
library(dplyr)
## #where is the folder
big_eval <- drive_find(pattern = "evaluate", type = "csv")#
head(big_eval)
path_eval <- drive_get(path = "Bignonieae/Bio10_enm")
path_eval
big_test_eval <-  drive_ls(path_eval, recursive = F)
big_test_eval
models_in_10bio <- big_test_eval$name
write.csv(models_in_10bio, "./output/enm/models_in_drive.csv")
