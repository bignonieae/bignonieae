---
title: "ENM methods"
author: "Andrea Sánchez-Tapia"
date: "`r Sys.time()`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Biotic data 
 + Raw data: `Localidades_X13.xlsx` (386 spp., n = 28873)
 + Data cleaning:
    + One occurrence per pixel per species (386 spp., n = 18441 @ 10min, n = 19722 @ 5min)
    + Species with n > 2 (354 spp., n = 18399)
 
##Environmental data

+ Chelsa
+ Resolution: 10min and  5min
+ Without Bio08, Bio09, Bio18, Bio19. 
+ Bounding box: 

```
                    xmin: -112 
                    xmax: -31 
                    ymin: -40 
                    ymax: 40 
```

+ PCA for variable generation (eigen) that explain at least 95% of the variation
    + 6 axes @ 10min
    + 6 axes @ 5min
    
## Modeling parameters

+ Pseudoabsence number: 
    + 10n when n < 10
    + 100nwhen n < 100
    + 10000 when n ≥ 100

+ Buffer distance:
    + Maximum distance for species with n<10
    + Median distance for species with n≥10
    
+ Partition type: Bootstrap, 5 times, 70% of the data
+ For eigen variables, all variables for every species
+ For bioclimatic variables, variable selection for each individual species, cutoff = 0.8.


 
 
 